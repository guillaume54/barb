use colored::*;
use colored_json::prelude::*;
use jsonformat::{format_json, Indentation};
use std::fmt::Display;

pub struct BarbOutput {
    request: bool,
    req_headers: bool,
    headers: bool,
    body: bool,
    raw_body: bool,
    color: bool,
}

impl BarbOutput {
    fn _print_header<T, I>(&self, name: T, value: I)
    where
        T: Display,
        I: Display,
    {
        println!("{}: {}", name, value);
    }

    pub fn new(
        request: bool,
        req_headers: bool,
        headers: bool,
        body: bool,
        raw_body: bool,
        color: bool,
    ) -> BarbOutput {
        BarbOutput {
            request,
            req_headers,
            headers,
            body,
            raw_body,
            color,
        }
    }

    #[cfg(test)]
    pub fn quiet() -> BarbOutput {
        BarbOutput {
            request: false,
            req_headers: false,
            headers: false,
            body: false,
            raw_body: false,
            color: false,
        }
    }

    pub fn req_dep(&self, method: String, url: String, code: u16, text: &str) {
        if !self.request {
            return;
        }
        if self.color {
            if code >= 400 {
                println!(
                    "{} {} {} {}",
                    code.to_string().red(),
                    text.red(),
                    method.red(),
                    url.red()
                );
            } else {
                println!(
                    "{} {} {} {}",
                    code.to_string().green(),
                    text,
                    method.purple(),
                    url.purple()
                );
            }
        } else {
            println!("{} {} {} {}", code, text, method, url);
        }
    }

    pub fn req(&self, method: String, url: String) {
        if !self.request {
            return;
        }
        if self.color {
            println!("{} {}", method.purple(), url.purple());
        } else {
            println!("{} {}", method, url);
        }
    }

    pub fn req_hdr(&self, name: String, value: String) {
        if !self.req_headers {
            return;
        }
        if self.color {
            self._print_header(name.yellow(), value);
        } else {
            self._print_header(name, value);
        }
    }

    pub fn end_req(&self) {
        if self.req_headers || self.request {
            println!();
        }
    }

    pub fn resp_hdr(&self, name: String, value: &str) {
        if !self.headers {
            return;
        }
        if self.color {
            self._print_header(name.blue(), value);
        } else {
            self._print_header(name, value);
        }
    }

    pub fn end_resp_hdr(&self) {
        if self.headers {
            println!();
        }
    }

    pub fn status(&self, code: u16, text: &str) {
        if !self.headers {
            return;
        }
        if self.color {
            if code >= 400 {
                println!("{} {}", code.to_string().red(), text.red());
            } else {
                println!("{} {}", code.to_string().green(), text);
            }
        } else {
            println!("{} {}", code, text);
        }
    }

    fn _format_body(&self, body: String) -> String {
        if self.raw_body {
            return body;
        }

        let formatted = format_json(body.as_str(), Indentation::Default);
        match self.color {
            true => formatted.to_colored_json_auto().unwrap_or(formatted),
            _ => formatted,
        }
    }

    pub fn body(&self, body: String) {
        if !self.body {
            return;
        }

        println!(
            "{}",
            match self.raw_body {
                true => body,
                _ => self._format_body(body),
            }
        );
    }
}
