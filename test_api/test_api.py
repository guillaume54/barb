from bottle import get, post, run, HTTPError, request

@get("/")
def get_root():
    return {"status": "OK"}

@get("/long")
def get_long():
    return {"data": [
        {"name": "apple", "calories": 5},
        {"name": "orange", "calories": 120},
        {"name": "pear", "calories": 45},
    ]}

@post("/")
def post_root():
    return {"status": "SUCCESS"}

@get("/errors/404")
def error_404():
    raise HTTPError(404, body={"error": "no exist"})

@get("/errors/500")
def error_500():
    raise HTTPError(500, body={"error": "server error"})

@post("/auth")
def post_auth():
    return {"token": "blah1234"}

@get("/profile")
def get_profile():
    if request.headers.get("TOKEN") != "blah1234":
        raise HTTPError(401, body={"error": "Not Authorized"})
    return {
        "user": "John Doe"
    }

run(host="localhost", port=8080)
