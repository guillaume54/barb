BARB
====

![Barb logo](doc/logo-readme.png)

Barb is a file-based API query tool that works nicely with version
control and fits into UNIX terminal usage.


<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [BARB](#barb)
    - [Installation](#installation)
    - [Example usage](#example-usage)
        - [CLI options](#cli-options)
    - [Barb format](#barb-format)
        - [Verb line](#verb-line)
        - [Headers](#headers)
        - [Filter](#filter)
            - [JSONPath](#jsonpath)
            - [JQ](#jq)
        - [Dependencies](#dependencies)
        - [Body](#body)
        - [Variable substitution](#variable-substitution)
            - [Placeholder format](#placeholder-format)
            - [Default value](#default-value)
    - [Credits](#credits)

<!-- markdown-toc end -->



## Installation

Barb is only available through `Cargo` at this time. To install the
default version with JSONPath only, [install rust with
rustup](https://rustup.rs).do like so:

```
cargo install barb
```

If you'd like to have JQ filtering; ensure the `libjq` is installed on
your machine, then run:

```
cargo install barb --features jq
```

## Example usage

```
barb [options] <file 1> <file 2> ... <file n>
```

### CLI options

- `-a, --all-headers`: Print all headers, request and response
- `-b, --body`: Only print the response body
- `-h, --headers`: Only print the response headers
- `-r, --raw`: Don't format the response body
- `-V, --version`: Print the software version
- `-n, --no-color`: Don't use color output
- `-f, --filter`: A JSON path to override any filters defined in the barb file
- `-F, --no-filter`: Disable all filters (except for dependencies)
- `--hdr <HDR>`: Set/override a header with format `NAME=VALUE`, can
  appear multiple times (does not affect dependencies)
- `--help`: Displays the help page

## Barb format

Barb uses a custom file format to perform requests. Each file contains
_one_ request and is started by a request preamble. Example:

```
#POST^http://my-blog.com/posts
#Authorization: TOKEN {API_TOKEN}
#$$.filter

{
    "title": "A post",
    "content": "My pretty blog post"
}

```

The preamble contains the directives relevant to performing the
request, such as the method, URL and headers. The preamble _must end
with an empty line_.

### Verb line

The verb line indicates to _barb_ what sort of request to perform and
where to. It follows this rigid format:

```
#<METHOD>^<URL>
```

The `URL` supports variable substitution, but `METHOD` does not.

Supported methods are:

- GET
- POST
- PUT
- DELETE
- PATCH

### Headers

Headers are formatted as follows:

```
#<HEADER NAME>: <HEADER VALUE>
```

The `HEADER VALUE` supports variable substitution, `HEADER NAME` does
not.

There can be none or many headers.

### Filter

Barb supports [JSONPath](https://goessner.net/articles/JsonPath/)
filtering by default, and optionally JQ.

#### JSONPath

Barb supports filtering of the response body with JSONPath. This has
the following format:

```
#$<JSON path>
```

The `PATH` supports variable substitution. Refer to the
[JSONPath](https://goessner.net/articles/JsonPath/) for more
information on the filters and their syntax.

Filters can be named to populate execution variables by extracting
values. Consider the following that will set the value of variable
FOOBAR:

```
#FOOBAR$<JSON path>
```

#### JQ

Barb supports JQ filtering of the response body. This has the
following format:

```
#|<JQ FILTER>
```

The `JQ FILTER` supports variable substitution. Refer to the [JQ
manual](https://stedolan.github.io/jq/manual/#Basicfilters) for more
information on the filters and their syntax.

Filters can be named to populate execution variables by extracting
values. Consider the following that will set the value of variable
FOOBAR:

```
#FOOBAR|<JQ FILTER>
```

### Dependencies

A barb file can declare only _one_ dependency which will be executed
before the main file is executed. If multiple dependencies are
declared, only the last one will be executed.

Syntax:

```
#>relative/path/to/file.barb
```

The path to the dependency can either be relative to the current file
or absolute. When running multiple barb files which have the same
dependency, that dependency will only be executed _once_.

A barb dependency _cannot have dependencies of its own_. Any
dependency declared within a dependency will simply be ignored.

### Body

Anything after the preamble is considered as a body and will be send
in the request for the following methods:

- PUT
- POST
- PATCH

Body does not support variable substitution.

### Variable substitution

Barb can include environment variable values and variables defined in
`.env` into the requests with the following placeholder format:

#### Placeholder format

```
{VARIABLE NAME}
```

This allows to do the following:

```
$ export BASE_URL="http://127.0.0.1:8000"
$ cat api-status.barb
#GET^{BASE_URL}/api/v1/status

$ barb api-status.barb
GET http://127.0.0.1:8000/api/v1/status

200 OK

{"status": "OK"}
```

#### Default value

A placholder can be given a default value that will be used if the
environment variable is not available. The format is as follows:

```
{VARIABLE NAME:-DEFAULT}
```

Example:

```
$ cat api-ping.barb
#GET^http://{HOST:-foobar.com}/api/ping

$ barb api-ping.barb
GET http://foobar.com/api/ping

200 OK

{"response": "pong"}

$ HOST=bar.com barb api-ping.barb
GET http://bar.com/api/ping

200 OK

{"response": "pong"}
```

## Credits

- Code: [Guillaume Pasquet](https://gitlab.com/guillaume54/)
- Logo: [Harpoon Chain Icon](https://game-icons.net/1x1/lorc/harpoon-chain.html) by Lorc under CC By 3.0

